let cards = [];
let cardNum = [2, 3, 4, 5, 6, 7, 8, 9, 10];
let cardFace = ["J", "Q", "K"];
let cardImg = ["jack", "queen", "king"];
let cardSuits = ["clubs", "diamonds", "hearts", "spades"];

function makeNumericCard(number, mainArr, suit) {
  for (let i = 0; i < number.length; i++) {
    for (let k = 0; k < suit.length; k++) {
      mainArr.push(`<div class="card">
        <div class="card__info">
          ${number[i]}
          <img src="images/${suit[k]}.svg" alt="${number[i]}, ${suit[k]}">
        </div>
        <div class="card__info">
          ${number[i]}
          <img src="images/${suit[k]}.svg" alt="${number[i]}, ${suit[k]}">
        </div>
      </div>`);
    }
  }
}

function makeFaceCard(face, mainArr, suit, img) {
  for (let i = 0; i < face.length; i++) {
    for (let k = 0; k < suit.length && img.length; k++) {
      mainArr.push(`<div class="card card--person">
        <div class="card__info">
          ${face[i]}
          <img src="images/${suit[k]}.svg" alt="${face[i]}, ${suit[k]}">
        </div>
        <img class="person" src="images/${img[i]}.svg" alt="${face[i]}, ${suit[k]}" />
        <div class="card__info">
          ${face[i]}
          <img src="images/${suit[k]}.svg" alt="${face[i]}, ${suit[k]}">
        </div>
      </div>`);
    }
  }
}

function makeAces(mainArr, suit) {
  for (let k = 0; k < suit.length; k++) {
    mainArr.push(`<div class="card card--person">
        <div class="card__info"> A
          <img src="images/${suit[k]}.svg" alt="${suit[k]}">
        </div>
        <img class="person" src="images/${suit[k]}.svg" alt="${suit[k]}" />
        <div class="card__info"> A
          <img src="images/${suit[k]}.svg" alt="${suit[k]}">
        </div>
      </div>`);
  }
}

makeNumericCard(cardNum, cards, cardSuits);
makeFaceCard(cardFace, cards, cardSuits, cardImg);
makeAces(cards, cardSuits);

document.write(`<div class="wrapper">${cards.join("")}</div>`);
